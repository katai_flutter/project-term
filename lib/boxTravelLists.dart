import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class BoxTravelLists extends StatelessWidget {
  String travel_id = "";
  String travel_title = "";

  BoxTravelLists(this.travel_id, this.travel_title);

  final Stream<QuerySnapshot> _travelStream =
      FirebaseFirestore.instance.collection('travelLists_north').snapshots();

  CollectionReference travelLists =
      FirebaseFirestore.instance.collection("travelLists_north");

  Future<void> deleteTravelLists(travel_id) {
    return travelLists
        .doc(travel_id)
        .delete()
        .then((value) => print("ลบข้อมูลสถานท่องเที่ยว"))
        .catchError((error) => print("พบปัญหาในการลบข้อมูล: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
            // padding: const EdgeInsets.only(bottom: 10),
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                decoration: BoxDecoration(
                    color: Colors.blue[100],
                    borderRadius: BorderRadius.circular(20)),
                width: MediaQuery.of(context).size.width * 0.95,
                height: 50,
                child: Row(
                  children: [
                    Expanded(
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                          Text(travel_title,
                              style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: "Fontcraft",
                                  fontWeight: FontWeight.w300),
                              textAlign: TextAlign.start),
                        ])),
                    IconButton(
                      icon: Icon(
                        Icons.delete,
                        size: 20,
                      ),
                      onPressed: () async {
                        await deleteTravelLists(travel_id);
                      },
                    )
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
