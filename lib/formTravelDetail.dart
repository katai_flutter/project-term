import 'dart:io';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:file_picker/file_picker.dart';

class FormTravelDetail extends StatefulWidget {
  String travel_id;
  String travel_title;
  String travel_image;
  String travel_detail;
  FormTravelDetail(
      {Key? key,
      required this.travel_id,
      required this.travel_title,
      required this.travel_image,
      required this.travel_detail})
      : super(key: key);

  @override
  _FormTravelDetailState createState() => _FormTravelDetailState(
      travel_id, travel_title, travel_image, travel_detail);
}

class _FormTravelDetailState extends State<FormTravelDetail> {
  String? _imageUrl;
  String travel_id = "";
  String travel_title = "";
  String travel_image = "";
  String travel_detail = "";
  CollectionReference travelLists =
      FirebaseFirestore.instance.collection('travelLists_north');
  _FormTravelDetailState(
      this.travel_id, this.travel_title, this.travel_image, this.travel_detail);

  TextEditingController _travelTitleController = new TextEditingController();
  TextEditingController _travelDetailControlleroller =
      new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (this.travel_id.isNotEmpty) {
      travel_title = travel_title;
      travel_detail = travel_detail;
      _travelTitleController.text = travel_title;
      _travelDetailControlleroller.text = travel_detail;
    } else {
      travel_image =
          "https://firebasestorage.googleapis.com/v0/b/projectexample-99395.appspot.com/o/thailand_travel%2Fno_image.png?alt=media&token=c3b3d753-1070-4cd1-9f39-1a9906c301e1";
    }
  }

  final _forKey = GlobalKey<FormState>();

  Future<void> addTravelLists() {
    return travelLists
        .add({
          'travel_title': this.travel_title,
          'travel_image': travel_image,
          'travel_detail': this.travel_detail
        })
        .then((value) => print("เพิ่มสถานที่ท่องเที่ยวสำเร็จ"))
        .catchError((error) => print("พบปัญหาในการเพิ่มข้อมูล: $error"));
  }

  Future<void> updateTravelLists() {
    return travelLists
        .doc(this.travel_id)
        .update({
          'travel_title': travel_title,
          'travel_image': this.travel_image,
          'travel_detail': this.travel_detail
        })
        .then((value) => print("แก้ไขข้อมูลสถานที่ท่องเที่ยวสำเร็จ"))
        .catchError((error) => print("พบปัญหาในการแก้ไขข้อมูล: $error"));
  }

  Future<void> uploadFile(PlatformFile file) async {
    try {
      // firebase_storage.UploadTask uploadTask;

      var ref = await firebase_storage.FirebaseStorage.instance
          .ref()
          .child('thailand_travel')
          .child('/${file.name}')
          .putFile(File(file.path!));
      String imageUrl = await ref.ref.getDownloadURL();
      print(imageUrl);

      setState(() {
        travel_image = imageUrl;
      });
    } on FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: [
          PopupMenuButton(
              itemBuilder: (context) => [
                    PopupMenuItem<int>(
                      value: 0,
                      child: Text("ออกจากระบบ"),
                    )
                  ])
        ]),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                      onPressed: () async {
                        if (_forKey.currentState!.validate()) {
                          if (travel_id.isEmpty) {
                            await addTravelLists();
                          } else {
                            await updateTravelLists();
                          }
                          Navigator.pop(context,
                              [travel_title, travel_image, travel_detail]);
                        }
                      },
                      child: Text("บันทึก"),
                      style: ElevatedButton.styleFrom(primary: Colors.green)),
                ),
              ),
              Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: _forKey,
                  child: Container(
                      child: Column(children: [
                    Container(
                        padding: const EdgeInsets.all(10),
                        child: TextFormField(
                          maxLength: 50,
                          decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'ชื่อสถานที่เที่ยว'),
                          controller: _travelTitleController,
                          onChanged: (value) {
                            setState(() {
                              travel_title = value;
                            });
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'กรุณากรอกชื่อสถานที่เทียว';
                            }
                            return null;
                          },
                        )),
                    Container(
                        child: Column(
                      children: [
                        ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Column(
                              children: [
                                Image.network(travel_image,
                                    width: 380, height: 220, fit: BoxFit.fill)
                              ],
                            )),
                        ElevatedButton(
                            onPressed: () async {
                              FilePickerResult? result =
                                  await FilePicker.platform.pickFiles();
                              if (result != null) {
                                PlatformFile file = result.files.single;
                                await uploadFile(file);
                              } else {}
                            },
                            child: Text("เลือกรูปภาพ"))
                      ],
                    )),
                    Container(
                        padding: const EdgeInsets.all(10),
                        child: TextFormField(
                            decoration: const InputDecoration(
                                border: UnderlineInputBorder(),
                                labelText: 'รายละเอียดสถานที่เที่ยว'),
                            minLines:
                                6, // any number you need (It works as the rows for the textarea)
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            controller: _travelDetailControlleroller,
                            onChanged: (value) {
                              setState(() {
                                travel_detail = value;
                              });
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'กรุณากรอกรายละเอียดของสถานที่เที่ยว';
                              }
                              return null;
                            }))
                  ])))
            ],
          ),
        ));
  }
}
