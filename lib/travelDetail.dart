import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'login.dart';
import 'formTravelDetail.dart';

class TravelDetail extends StatefulWidget {
  String travel_id;
  String travel_title;
  String travel_image;
  String travel_detail;
  TravelDetail(
      {Key? key,
      required this.travel_id,
      required this.travel_title,
      required this.travel_image,
      required this.travel_detail})
      : super(key: key);

  @override
  _TravelDetailState createState() =>
      _TravelDetailState(travel_id, travel_title, travel_image, travel_detail);
}

class _TravelDetailState extends State<TravelDetail> {
  String travel_id;
  String travel_title;
  String travel_image;
  String travel_detail;
  _TravelDetailState(
      this.travel_id, this.travel_title, this.travel_image, this.travel_detail);

  void onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        FirebaseAuth.instance.signOut();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => LoginPage()));
        break;
    }
  }

  void _setData(test) {
    setState(() {
      travel_title = test[0];
      travel_image = test[1];
      travel_detail = test[2];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: [
          PopupMenuButton<int>(
              onSelected: (item) => onSelected(context, item),
              itemBuilder: (context) => [
                    PopupMenuItem<int>(
                      value: 0,
                      child: Text("ออกจากระบบ"),
                    )
                  ])
        ]),
        body: Center(
            child: Container(
                child: Column(children: [
          Container(
              padding: const EdgeInsets.all(10),
              child: Text(travel_title, style: TextStyle(fontSize: 20))),
          Container(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Column(
                    children: [
                      Image.network(travel_image,
                          width: 380, height: 220, fit: BoxFit.fill)
                    ],
                  ))),
          Container(
              width: 380,
              padding: const EdgeInsets.all(10),
              child: Text(travel_detail, textAlign: TextAlign.start))
        ]))),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FormTravelDetail(
                          travel_id: travel_id,
                          travel_title: travel_title,
                          travel_image: travel_image,
                          travel_detail: travel_detail)))
              .then((returnValue) => _setData(returnValue)),
          tooltip: 'แก้ไขข้อมูล',
          child: const Icon(Icons.edit),
        ));
  }
}
