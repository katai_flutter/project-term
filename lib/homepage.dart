import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'login.dart';
import 'boxRegions.dart';
import 'travelLists.dart';

class HomepageTitle extends StatelessWidget {
  String homeTitle = ""; // ชื่อแอปในหน้าแรก

  HomepageTitle(this.homeTitle);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(20),
        child: Text(homeTitle, style: TextStyle(fontSize: 20)));
  }
}

class Homepage extends StatelessWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<BoxRegions> lists = [
      BoxRegions(
          "https://firebasestorage.googleapis.com/v0/b/projectexample-99395.appspot.com/o/thailand_travel%2F%E0%B9%80%E0%B8%AB%E0%B8%99%E0%B8%B7%E0%B8%AD.JPG?alt=media&token=c8d19d47-6723-48f0-b4ce-e9fc7b813a31",
          "ภาคเหนือ"),
      BoxRegions(
          "https://firebasestorage.googleapis.com/v0/b/projectexample-99395.appspot.com/o/thailand_travel%2F%E0%B8%81%E0%B8%A5%E0%B8%B2%E0%B8%87.JPG?alt=media&token=247c4172-80f5-47ca-8018-02ff61c4adb1",
          "ภาคกลาง"),
      BoxRegions(
          "https://firebasestorage.googleapis.com/v0/b/projectexample-99395.appspot.com/o/thailand_travel%2F%E0%B8%95%E0%B8%B0%E0%B8%A7%E0%B8%B1%E0%B8%99%E0%B8%AD%E0%B8%AD%E0%B8%81%E0%B9%80%E0%B8%89%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B9%80%E0%B8%AB%E0%B8%99%E0%B8%B7%E0%B8%AD.JPG?alt=media&token=a38b48f1-fb30-4afb-abb1-b93b7581dc7f",
          "ภาคตะวันออกเฉียงเหนือ"),
      BoxRegions(
          "https://firebasestorage.googleapis.com/v0/b/projectexample-99395.appspot.com/o/thailand_travel%2F%E0%B8%95%E0%B8%B0%E0%B8%A7%E0%B8%B1%E0%B8%99%E0%B8%AD%E0%B8%AD%E0%B8%81.JPG?alt=media&token=cade7299-2371-4095-823d-9d4ac1720839",
          "ภาคตะวันออก"),
      BoxRegions(
          "https://firebasestorage.googleapis.com/v0/b/projectexample-99395.appspot.com/o/thailand_travel%2F%E0%B8%95%E0%B8%B0%E0%B8%A7%E0%B8%B1%E0%B8%99%E0%B8%95%E0%B8%81.JPG?alt=media&token=2e4dbde6-de60-4459-b62a-b7a5385022d0",
          "ภาคตะวันตก"),
      BoxRegions(
          "https://firebasestorage.googleapis.com/v0/b/projectexample-99395.appspot.com/o/thailand_travel%2F%E0%B9%83%E0%B8%95%E0%B9%89.JPG?alt=media&token=9de97fc2-8bcb-48a9-8a62-9113b3565643",
          "ภาคใต้")
    ];

    return Scaffold(
        appBar: AppBar(automaticallyImplyLeading: false, actions: [
          PopupMenuButton<int>(
              onSelected: (item) => onSelected(context, item),
              itemBuilder: (context) => [
                    PopupMenuItem<int>(
                      value: 0,
                      child: Text("ออกจากระบบ"),
                    )
                  ])
        ]),
        body: Column(
          children: [
            HomepageTitle("สถานที่ท่องเที่ยวในประเทศไทย"),
            Expanded(
                child: GridView.builder(
                    itemCount: lists.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemBuilder: (BuildContext context, int index) {
                      BoxRegions listShow = lists[index];
                      return ListTile(
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [listShow],
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        TravelLists(region: listShow.text)));
                          });
                    }))
          ],
        ));
  }

  void onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        FirebaseAuth.instance.signOut();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => LoginPage()));
        break;
    }
  }
}
