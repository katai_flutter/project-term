import 'package:flutter/material.dart';

class BoxRegions extends StatelessWidget {
  String imagePath = "";
  String text = "";

  BoxRegions(this.imagePath, this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Column(
              children: [
                Image.network(imagePath,
                    width: 260,
                    height: 130,
                    fit: BoxFit.fill),
                Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    width: 260,
                    height: 50,
                    color: Colors.blue,
                    child: Text(text,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontFamily: "Fontcraft",
                            fontWeight: FontWeight.w300),
                        textAlign: TextAlign.start))
              ],
            )));
  }
}
