import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'travelDetail.dart';
import 'formTravelDetail.dart';
import 'login.dart';
import 'boxTravelLists.dart';

class TravelLists extends StatefulWidget {
  String region;
  TravelLists({Key? key, required this.region}) : super(key: key);

  @override
  _TravelListsState createState() => _TravelListsState(region);
}

class _TravelListsState extends State<TravelLists> {
  String region;
  _TravelListsState(this.region);
  
  final Stream<QuerySnapshot> _travelStream =
      FirebaseFirestore.instance.collection('travelLists_north').snapshots();

  CollectionReference travelLists =
      FirebaseFirestore.instance.collection("travelLists_north");

  void onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        FirebaseAuth.instance.signOut();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => LoginPage()));
        break;
    }
  }

  Future<void> deleteUser(travel_id) {
    return travelLists
        .doc(travel_id)
        .delete()
        .then((value) => print("ลบข้อมูลสถานท่องเที่ยว"))
        .catchError((error) => print("พบปัญหาในการลบข้อมูล: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _travelStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('พบปัญหา');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("กำลังโหลด...");
        }

        return Scaffold(
            appBar: AppBar(actions: [
              PopupMenuButton<int>(
                  onSelected: (item) => onSelected(context, item),
                  itemBuilder: (context) => [
                        PopupMenuItem<int>(
                          value: 0,
                          child: Text("ออกจากระบบ"),
                        )
                      ])
            ]),
            body: Column(children: [
              Container(
                  padding: const EdgeInsets.all(20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(region, style: TextStyle(fontSize: 20)),
                      Tooltip(
                        message: "เพิ่มสถานที่ท่องเที่ยว",
                        child: ElevatedButton(
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormTravelDetail(
                                        travel_id: "",
                                        travel_title: "",
                                        travel_image: "",
                                        travel_detail: ""))),
                            child: const Icon(Icons.add),
                            style: ElevatedButton.styleFrom(
                              shape: CircleBorder(),
                            )),
                      )
                    ],
                  )),
              Expanded(
                  child: ListView(
                children: snapshot.data!.docs.map((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  BoxTravelLists listShow =
                      BoxTravelLists(document.id, data['travel_title']);
                  return ListTile(
                      title: Column(
                        children: [listShow],
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TravelDetail(
                                    travel_id: document.id,
                                    travel_title: data['travel_title'],
                                    travel_image: data['travel_image'],
                                    travel_detail: data['travel_detail'])));
                      });
                }).toList(),
              ))
            ]));
      },
    );
  }
}
