import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/ui/utils/stream_subscriber_mixin.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:thailand_travel/homepage.dart';
import 'formTravelDetail.dart';
import 'login.dart';
import 'uploadFile.dart';
import 'travelLists.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  late StreamSubscription<User?> _sub;
  @override
  void initState() {
    super.initState();
    _sub = FirebaseAuth.instance.authStateChanges().listen((user) {
      print(user!.displayName);
      _navigatorKey.currentState!
          .pushReplacementNamed(user != null ? "home" : "login");
    });
  }

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
            primaryColor: Colors.lightBlue[100], fontFamily: "Fontcraft"),
        debugShowCheckedModeBanner: false,
        navigatorKey: _navigatorKey,
        initialRoute:
            FirebaseAuth.instance.currentUser == null ? "login" : "home",
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case "home":
              return MaterialPageRoute(
                  settings: settings, builder: (_) => Homepage());
            case "login":
              return MaterialPageRoute(
                  settings: settings, builder: (_) => LoginPage());
          }
        });
  }
}
